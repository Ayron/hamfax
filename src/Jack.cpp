 /****************************************************************************
 *                                                                           *
 *                                                      ,8                   *
 *                                                    ,d8*                   *
 *                                                  ,d88**                   *
 *                                                ,d888`**                   *
 *                                             ,d888`****                    *
 *                                            ,d88`******                    *
 *                                          ,d88`*********                   *
 *                                         ,d8`************                  *
 *                                       ,d8****************                 *
 *                                     ,d88**************..d**`              *
 *                                   ,d88`*********..d8*`****                *
 *                                 ,d888`****..d8P*`********                 *
 *                         .     ,d8888*8888*`*************                  *
 *                       ,*     ,88888*8P*****************                   *
 *                     ,*      d888888*8b.****************                   *
 *                   ,P       dP  *888.*888b.**************                  *
 *                 ,8*        8    *888*8`**88888b.*********                 *
 *               ,dP                *88 8b.*******88b.******                 *
 *              d8`                  *8b 8b.***********8b.***                *
 *            ,d8`                    *8. 8b.**************88b.              *
 *           d8P                       88.*8b.***************                *
 *         ,88P                        *88**8b.************                  *
 *        d888*       .d88P            888***8b.*********                    *
 *       d8888b..d888888*              888****8b.*******        *            *
 *     ,888888888888888b.              888*****8b.*****         8            *
 *    ,8*;88888P*****788888888ba.      888******8b.****      * 8' *          *
 *   ,8;,8888*         `88888*         d88*******8b.***       *8*'           *
 *   )8e888*          ,88888be.       888*********8b.**       8'             *
 *  ,d888`           ,8888888***     d888**********88b.*    d8'              *
 * ,d88P`           ,8888888Pb.     d888`***********888b.  d8'               *
 * 888*            ,88888888**   .d8888*************      d8'                *
 * `88            ,888888888    .d88888b*********        d88'                *
 *               ,8888888888bd888888********             d88'                *
 *               d888888888888d888********                d88'               *
 *               8888888888888888b.****                    d88'              *
 *               88*. *88888888888b.*      .oo.             d888'            *
 *               `888b.`8888888888888b. .d8888P               d888'          *
 *                **88b.`*8888888888888888888888b...            d888'        *
 *                 *888b.`*8888888888P***7888888888888e.          d888'      *
 *                  88888b.`********.d8888b**  `88888P*            d888'     *
 *                  `888888b     .d88888888888**  `8888.            d888'    *
 *                   )888888.   d888888888888P      `8888888b.      d88888'  *
 *                  ,88888*    d88888888888**`        `8888b          d888'  *
 *                 ,8888*    .8888888888P`              `888b.        d888'  *
 *                ,888*      888888888b...                `88P88b.  d8888'   *
 *       .db.   ,d88*        88888888888888b                `88888888888     *
 *   ,d888888b.8888`         `*888888888888888888P`              ******      *
 *  /*****8888b**`              `***8888P*``8888`                            *
 *   /**88`                               /**88`                             *
 *   `|'                             `|*8888888'                             *
 *                                                                           *
 * Jack.cpp                                                                  *
 *                                                                           *
 * Jack Connector                                                            *
 *                                                                           *
 * Copyright (c) 2019 Dark Ayron, DL7AYR <ayron@shadowdrake.fur>             *
 *                                                                           *
 *****************************************************************************
 *                                                                           *
 *  This program is free software; you can redistribute it and/or modify     *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation; either version 2 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License along  *
 *  with this program; if not, write to the Free Software Foundation, Inc.,  *
 *  51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.                  *
 *                                                                           *
 ****************************************************************************/

#include "config.h"

#ifdef USE_JACK

#include "Jack.hpp"
#include <string.h>
#include <stdint.h>
#include <stdexcept>
#include <stdio.h>
#include <unistd.h>

static int16_t convbuffer[8192];

Jack::Jack(QObject*parent)
	:QObject(parent), enabled(0), connected(0)
{
	jack_status_t status;
	client = jack_client_open("hamfax", JackNoStartServer, &status);
	if (!client) {
		throw std::runtime_error("Can't connect to jack.");
	}
	inPort = jack_port_register(client, "in", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput | JackPortIsTerminal, 0);
	outPort = jack_port_register(client, "out", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput | JackPortIsTerminal, 0);

	if ((!inPort) || (!outPort)) {
		jack_client_close(client);
		throw std::runtime_error("Can't create jack ports.");
	}

	inBuffer = jack_ringbuffer_create(12288);
	if (!inBuffer) {
		jack_client_close(client);
		throw std::runtime_error("Can't create jack ringbuffer.");
	}
	outBuffer = jack_ringbuffer_create(12288);
	if (!outBuffer) {
		jack_ringbuffer_free(inBuffer);
		jack_client_close(client);
		throw std::runtime_error("Can't create jack ringbuffer.");
	}
}

Jack::~Jack()
{
	jack_ringbuffer_free(inBuffer);
	jack_ringbuffer_free(inBuffer);
	jack_client_close(client);
}

void Jack::activate()
{
	jack_set_process_callback(client, Jack::jackProcess, this);
	jack_set_buffer_size_callback(client, Jack::jackBufferSize, this);
	jack_activate(client);
}

void Jack::deactivate()
{
	jack_deactivate(client);
}

void Jack::setEnabled(int enabled)
{
	this->enabled = enabled;
}

int Jack::isEnabled()
{
	return enabled;
}

uint32_t Jack::getSampleRate()
{
	return jack_get_sample_rate(client);
}

jack_ringbuffer_t*Jack::getRingbuffer(int which)
{
	switch (which) {
		case JACK_INPUT:
			return inBuffer;
		case JACK_OUTPUT:
			return outBuffer;
		default:
			return 0;
	}
}

jack_nframes_t Jack::getBufferSize()
{
	return bufferSize;
}

int Jack::jackProcess(jack_nframes_t nframes, void*arg)
{
	Jack*jack_obj = (Jack*)arg;
	float*in;
	float*out;
	uint32_t n;

	in = (float*)jack_port_get_buffer(jack_obj->inPort, nframes);
	out = (float*)jack_port_get_buffer(jack_obj->outPort, nframes);

	if (jack_ringbuffer_read_space(jack_obj->outBuffer) >= nframes * 2) {
		jack_ringbuffer_read(jack_obj->outBuffer, (char*)convbuffer, nframes * 2);
		for (n = 0; n < nframes; n++) {
			out[n] = convbuffer[n] / 32768.0;
		}
	} else {
		memset(out, 0, nframes * 4);
	}

	if (jack_obj->enabled && (jack_ringbuffer_write_space(jack_obj->inBuffer) > nframes * 2)) {
		for (n = 0; n < nframes; n++) {
			/* handle clipping */
			if (in[n] > 1.0)
				convbuffer[n] = 32767;
			else if (in[n] < -1.0)
				convbuffer[n] = -32767;
			else
				convbuffer[n] = in[n] * 32767.0;
		}
		jack_ringbuffer_write(jack_obj->inBuffer, (char*)convbuffer, nframes * 2);
	}

	if (jack_obj->enabled) {
		emit jack_obj->data();
	}

	return 0;
}

int Jack::jackBufferSize(jack_nframes_t nframes, void*arg)
{
	Jack*jack_obj = (Jack*)arg;

	jack_obj->bufferSize = nframes;
	return 0;
}

#endif /* USE_JACK */

